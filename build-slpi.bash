#!/bin/bash
set -e -x

export HEXAGON_SDK_DIR=/opt/hexagon-sdk/4.1.0.4-lite

echo "[INFO] Starting SLPI build"
cd slpi
rm -rf build
mkdir build
cd build
cmake ../
make
cd ../../
echo "[INFO] Finished SLPI build"
