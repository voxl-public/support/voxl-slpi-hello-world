#include <stddef.h>
extern "C" size_t strnlen_s(const char *s, size_t n) {
	if (s == NULL) return 0;
	for (size_t i = 0; i < n; i++) {
		if (s[i] == '\0') {
			return i;
		}
	}
	return n;
}

extern "C" void free(void *ptr) {
	return;
}

// malloc
extern "C" void *malloc(size_t size) {
	return NULL;
}

// posix_memalign
extern "C" int posix_memalign(void **memptr, size_t alignment, size_t size) {
	return 0;
}

// calloc
extern "C" void *calloc(size_t nmemb, size_t size) {
	return NULL;
}


// realloc
extern "C" void *realloc(void *ptr, size_t size) {
	return NULL;
}

// nanosleep
#include <time.h>
extern "C" int nanosleep(const struct timespec *req, struct timespec *_Nullable rem) {
	return 0;
}
