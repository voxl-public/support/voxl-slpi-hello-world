#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <qurt.h>
#include <new>

// TODO: This has to be defined in the slpi_proc build and in this build.
// Make it accessible from one file to both builds.
typedef struct {
    int (*advertise_func_ptr)(const char *topic_name);
    int (*subscribe_func_ptr)(const char *topic_name);
    int (*unsubscribe_func_ptr)(const char *topic_name);
    int (*topic_data_func_ptr)(const char *name, const uint8_t *data, int data_len_in_bytes);
    int (*config_spi_bus)();
    int (*spi_transfer)(int fd, const uint8_t *send, uint8_t *recv, const unsigned len);
    int (*config_i2c_bus)(uint8_t bus_number, uint8_t address, uint32_t frequency);
    void (*set_i2c_address)(int fd, uint8_t address);
    int (*i2c_transfer)(int fd, const uint8_t *send, const unsigned send_len, uint8_t *recv, const unsigned recv_len);
    int (*config_uart)(uint8_t port_number, uint32_t speed);
    int (*uart_write)(int fd, const char *data, const unsigned data_len);
    int (*uart_read)(int fd, char *buffer, const unsigned buffer_len);
    int (*register_interrupt_callback)(int (*)(int, void*, void*), void* arg);
} fc_func_ptrs;

class hello_world_count {
public:
    void increment() { _count++; }
    int  get_count() { return _count; }

private:
    int _count;
};

extern "C" void HAP_debug(const char *msg, int level, const char *filename, int line);

#define PRINT_BUFFER_SIZE 256
char print_buffer[PRINT_BUFFER_SIZE];

void debug_print(int level, const char* fmt, ...)
{
  memset(&print_buffer,0, PRINT_BUFFER_SIZE);

  va_list args;
  va_start(args,fmt);
  int len = vsnprintf(print_buffer, PRINT_BUFFER_SIZE, fmt, args);
  va_end(args);

  HAP_debug(print_buffer, level, __FILE__, __LINE__);
}

#define THREAD_STACK_SIZE 8192
static char thread_stack[THREAD_STACK_SIZE];

static fc_func_ptrs _func_ptrs;

static bool thread_running = false;

static bool got_first_topic_data = false;

static void hello_world_tx_thread(void *test) {
    debug_print(1, "hello_world_tx_thread starting");

    hello_world_count count;

    while (true) {
        if ((got_first_topic_data) && (_func_ptrs.topic_data_func_ptr)) {
            const char msg[] = "Hello, world! from SLPI!";
            if (_func_ptrs.topic_data_func_ptr("debug_print", (const uint8_t*) msg, strnlen_s(msg, 256)) != 0) {
                debug_print(1, "topic_data_func_ptr failed");
            } else {
                debug_print(1, "topic_data_func_ptr succeeded");
            }
        } else {
            debug_print(1, "Null topic_data_func_ptr");
        }

        count.increment();

        qurt_timer_sleep(1 * 1000 * 1000);
    }

    thread_running = false;

    debug_print(1, "hello_world_tx_thread exiting. Count: %d", count.get_count());

	qurt_thread_exit(0);
}

#define __EXPORT __attribute__ ((visibility ("default")))
extern "C" {

	int px4muorb_orb_initialize(fc_func_ptrs *func_ptrs, int32_t clock_offset_us) __EXPORT;

	int px4muorb_topic_advertised(const char *name) __EXPORT;

	int px4muorb_add_subscriber(const char *name) __EXPORT;

	int px4muorb_remove_subscriber(const char *name) __EXPORT;

	int px4muorb_send_topic_data(const char *name, const uint8_t *data, int data_len_in_bytes) __EXPORT;

	float px4muorb_get_cpu_load(void) __EXPORT;
}

void operator delete(void* p) noexcept {
	free(p);
}

void check_for_zero(uint8_t *buffer, int len) {
	for (int i = 0; i < len; i++) {
		if (buffer[i] != 0) {
			debug_print(1, "buffer not zero (%u) at offset %d", buffer[i], i);
		}
	}
}

struct mem_test_node {
	uint8_t data[1024 - sizeof(void*)];
	void *next;
};

void* test_new_null_handling() {
	struct mem_test_node* test_new_null_ptr = new struct mem_test_node;
	if (test_new_null_ptr == nullptr) {
		return nullptr;
	}
	return (void*) test_new_null_ptr;
}

void new_test_woe(void) {
	debug_print(1, "Starting new test without exceptions");

	uint32_t counter = 1;

	struct mem_test_node* new_test_head = (struct mem_test_node*) new (std::nothrow) struct mem_test_node;
	struct mem_test_node* new_test_current = new_test_head;
	if (new_test_head == NULL) {
		debug_print(1, "new test failed to allocate any memory");
		return;
	}
	struct mem_test_node* temp = (struct mem_test_node*) new struct mem_test_node;
	while (temp != NULL) {
		counter++;
		new_test_current->next = (void*) temp;
		new_test_current = temp;
		temp = (struct mem_test_node*) new (std::nothrow) struct mem_test_node;
	}
	debug_print(1, "new test allocated %u buffers", counter);
	new_test_current = new_test_head;
	while (true) {
		if (new_test_current->next != NULL) {
			new_test_current = (struct mem_test_node*) new_test_current->next;
			delete new_test_head;
			new_test_head = new_test_current;
		} else {
			delete new_test_current;
			break;
		}
	}
}

void new_test_we(void) {
	debug_print(1, "Starting new test with exceptions");

	uint32_t counter = 1;

	//  struct mem_test_node* new_test_head = (struct mem_test_node*) new struct mem_test_node;
	struct mem_test_node* new_test_head = (struct mem_test_node*) test_new_null_handling();
	struct mem_test_node* new_test_current = new_test_head;
	if (new_test_head == NULL) {
		debug_print(1, "new test failed to allocate any memory");
		return;
	}
	struct mem_test_node* temp = (struct mem_test_node*) new struct mem_test_node;
	while (temp != NULL) {
		counter++;
		new_test_current->next = (void*) temp;
		new_test_current = temp;
		try {
			temp = (struct mem_test_node*) new struct mem_test_node;
		}
		// catch(std::bad_alloc) {
		catch(...) {
			debug_print(1, "Got new exception bad_alloc");
			qurt_timer_sleep(5000);
			break;
		}
	}
	debug_print(1, "new test allocated %u buffers", counter);
	new_test_current = new_test_head;
	while (true) {
		if (new_test_current->next != NULL) {
			new_test_current = (struct mem_test_node*) new_test_current->next;
			delete new_test_head;
			new_test_head = new_test_current;
		} else {
			delete new_test_current;
			break;
		}
	}
}

void calloc_test(void) {
	debug_print(1, "Starting calloc test");

	uint32_t counter = 1;

	struct mem_test_node* calloc_test_head = (struct mem_test_node*) calloc(sizeof(struct mem_test_node), 1);
	struct mem_test_node* calloc_test_current = calloc_test_head;
	if (calloc_test_head == NULL) {
		debug_print(1, "calloc test failed to allocate any memory");
		return;
	}
	check_for_zero((uint8_t*) calloc_test_head, sizeof(struct mem_test_node));
	struct mem_test_node* temp = (struct mem_test_node*) calloc(sizeof(struct mem_test_node), 1);
	while (temp != NULL) {
		counter++;
		check_for_zero((uint8_t*) temp, sizeof(struct mem_test_node));
		calloc_test_current->next = (void*) temp;
		calloc_test_current = temp;
		temp = (struct mem_test_node*) calloc(sizeof(struct mem_test_node), 1);
	}
	debug_print(1, "calloc test allocated %u buffers", counter);
	calloc_test_current = calloc_test_head;
	while (true) {
		if (calloc_test_current->next != NULL) {
			calloc_test_current = (struct mem_test_node*) calloc_test_current->next;
			free(calloc_test_head);
			calloc_test_head = calloc_test_current;
		} else {
			free(calloc_test_current);
			break;
		}
	}
}

class ValueTest {
public:
	ValueTest(int val) : _value(val) {};
	~ValueTest() {};

	int getValue() const { return _value; }

private:
	int _value;
};

const ValueTest& get_ValueTest(int init) {
    static const ValueTest *vt;
    if (vt == nullptr) {
        vt = new ValueTest(init);
    }
    return *vt;
}

const ValueTest& myValue = get_ValueTest(5);

int px4muorb_orb_initialize(fc_func_ptrs *func_ptrs, int32_t clock_offset_us)
{
    (void) clock_offset_us;

    debug_print(1, "The value is %d", myValue.getValue());

    if (func_ptrs != NULL) {
        _func_ptrs = *func_ptrs;
    } else {
        return -1;
    }

    qurt_thread_t tid;
    qurt_thread_attr_t attr;

    if ( ! thread_running) {
        qurt_thread_attr_init(&attr);
        qurt_thread_attr_set_stack_addr(&attr, thread_stack);
        qurt_thread_attr_set_stack_size(&attr, THREAD_STACK_SIZE);
        int status = qurt_thread_create(&tid, &attr, &hello_world_tx_thread, NULL);

        if (status == QURT_EFAILED) {
            debug_print(1, "Error starting hello world transmit thread");
            return -1;
        } else {
            debug_print(1, "Starting hello world transmit thread");
            thread_running = true;
        }
    }
    else
    {
        debug_print(1, "hello world transmit thread is already running");
    }

	calloc_test();
	new_test_we();
	new_test_woe();

	return 0;
}

int px4muorb_topic_advertised(const char *topic_name)
{
	return 0;
}

int px4muorb_add_subscriber(const char *topic_name)
{
	return 0;
}

int px4muorb_remove_subscriber(const char *topic_name)
{
	return 0;
}

//this function is called with data from CPU
int px4muorb_send_topic_data(const char *topic_name, const uint8_t *data,
			     int data_len_in_bytes)
{
    got_first_topic_data = true;

    if (strcmp("debug_print", topic_name) == 0) {
        debug_print(1, "Got: %s", data);
    } else {
        debug_print(1, "Got unknown command: %s", topic_name);
    }

	return 0;
}

