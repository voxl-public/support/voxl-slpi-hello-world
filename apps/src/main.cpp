
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#include "fc_sensor.h"

static int initialized = 0;
bool enable_debug = false;

static void receive_callback(const char *topic,
                      const uint8_t *data,
                      uint32_t length_in_bytes) {

    if (strcmp("debug_print", topic) == 0) {
        printf("Got: %s\n", data);
    } else {
        fprintf(stderr, "Got unknown command: %s\n", topic);
    }
}

static void unused_callback(const char *topic)
{
    printf("Got unused callback for topic %s\n", topic);
}

int main() {
    fc_callbacks cb = {&receive_callback, &unused_callback,
                       &unused_callback, &unused_callback};

    if (fc_sensor_set_library_name("libslpi_hello_world.so") != 0) {
        fprintf(stderr, "Error setting library name\n");
        return -1;
    }

	printf("About to call init\n");

    if (fc_sensor_initialize(enable_debug, &cb) != 0) {
        fprintf(stderr, "Error calling fc_sensor_initialize\n");
    	fc_sensor_kill_slpi();
        return -1;
    } else if (enable_debug) {
        printf("fc_sensor_initialize succeeded\n");
    }

    const char msg[] = "Hello, world! from APPS!";
    for (int i = 0; i < 5; i++) {
        int status = fc_sensor_send_data("debug_print", (const uint8_t*) msg, strlen(msg));

        sleep(1);

		printf("Sleeping 1 second\n");
        
        if (status) {
            fprintf(stderr, "fc_sensor_send_data failed\n");
            break;
        }
    }

    // Send reset to SLPI
    fc_sensor_kill_slpi();

    sleep(1);

    return 0;
}

