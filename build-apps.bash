#!/bin/bash
set -e -x

export AARCH64_SDK_DIR=/opt/aarch64-sdk/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu

echo "[INFO] Starting APPS build"
cd apps
rm -rf build
mkdir build
cd build
cmake ../
make
cd ../../
echo "[INFO] Finished APPS build"
